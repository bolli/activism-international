# Privacy Policy for temporary services 

This policy is relevant for 
* [Pads](https://pad.activism.international)
* [Whiteboards](https://board.activism.international)
* [Jitsi](https://meet.activism.international)
* [BigBlueButton (without login)](https://bbb.activism.international/easy/))
* [Livestreams (when not streaming)](https://live.activism.international)

Privacy is important. That is why we try to reduce the amount of data we collect as much as we can. Data economy means, the fewer data we collect, the fewer data we need to protect.

## Responsibility for data processing

The maintainers listed in the [legal notice](../about.md) are responsible for any processing of personal data.

## Affected users

We do not store identifiable or personal data of yours unless you explicitly provide us with such. Anyway, there issome kinds of general data we process.

As soon as you actively create any content in your temporary online services, you are affected of automated data processing. Content could be:

- Writing a text message in the Jitsi chat,
- Writing a text message in the BigBlueButton chat,
- Writing content into a pad,
- Writing a text message in a pad's chat.
- Writing a text message in the livestreams chat.

Note: Video and audio shown in Jitsi calls or BigBlueButton meetings are only transferred by our servers but not saved.

Initiators of BigBlueButton meetings have the option to stream or record meetings. In this case, you will be informed before joining the meeting and just before the recording starts. For further use of the recordings, refer to the meeting's creator.

## Expiry of stored data

- Recordings of BigBlueButton meetings are stored for one month
- Pads are deleted after one month of inactivity
- Chat messages in all our temporary services are being deleted immediately (or after 5 hours for chat messages during a livestream) after the meeting or collaborative editing ended

## Reasons for processing data

You actively created content using our online services.

## Transfer to thirds

We only transfer data to thirds, if you ask us to. In no case, we transfer usable or personally identifiable data.

- If you enter your mail address or Matrix-ID in a Jitsi call, we integrate with [Gravatar](https://gravatar.com/) to check your profile picture. For this, your mail or MXID is being hashed. This means, no one can detect you actual mail address or MXID. For example, `info@activism.international` becomes `8af080f8a07e73bb8f7a391ce738807c` that way. No personal data is being transferred.

## Embedded contents

In Jitsi or BigBlueButton meetings, the moderators can embed other websites, videos or slides into the meeting. As it is up to them which services they integrate, we cannot tell you anything about the services' privacy. Please ask your meeting's moderator to provide the given information.

## Cookies

We use cookies to store session information and other necessary information on your local device. This information could be e.g. the last meetings you attended, the last pads you used or the display name you used in the last meeting. We do not attempt to track you.

## Analytics services

We do not use analytics services.

## Hosting

Our servers are located in the EU and are protected of physical access of thirds.

## Your rights

Even though we try to minimize the personal data we process, we would like to explain you your rights according to GDPR:

You have various rights.

- We fulfill your right on access according to Art. 15. As we do not have any identifiable data of yours, you can request a copy of the data we do not have as an empty mail.
- According to Art. 16, you can request to correct the nonexistent personal data we collect.
- According to Art. 17, you can request us to erase all personal data of yours. For chat messages, we promise we continuously do this even before you ask us to. If you want a confirmation of this, feel free to email us. We can confirm the automatic erasure by a thumbs up in a mail. You can request the deletion of pads at any time by mail.
- According to Art. 18, you can request us to decrease the amount or to cherry-pick the kind of personal data we process. Due to physical reasons, we obviously cannot process less than no data.
- According to Art. 20, you can request us to transfer all personal data we save (hence nothing)
- You are allowed to withdraw your consent any time. You can always deny future processing of data.

Independent of the administration or legal orders, you are always free to create an appeal at the competent supervisory authority, especially in the country of abode, your work or the presumed violation, if you assume our processing of personal data violates the GDPR.
